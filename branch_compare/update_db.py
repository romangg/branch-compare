#!/usr/bin/env python

from dateutil.parser import parse as parsedate
from datetime import datetime
from datetime import UTC
from pathlib import Path
import requests
import tarfile
import json
import subprocess
import tempfile
import re


# Program constant parameters
MIRROR = 'https://manjaro.kyberorg.fi'
BRANCHES = ('stable', 'testing', 'unstable')
ARCHS = ['x86_64', 'aarch64']
MIRROR_BRANCHES = {
    'x86_64': BRANCHES,
    'aarch64': ('arm-stable', 'arm-testing', 'arm-unstable'),
}
REPOS = {
    'x86_64': ('core', 'community', 'extra', 'multilib'),
    'aarch64': ('core', 'community', 'extra', 'mobile', 'kde-unstable'),
}
BASE_DIR = Path(__file__).parent
CACHE_DIR = BASE_DIR / 'cache'
DB_DIR = BASE_DIR

def read_packages(packages: dict, repo: str, branch: str) -> None:
    '''Read a repo's database file (tar.gz) and create a list of packages in-memory.
    The dictionary in the 'packages' parameter get filled with the packages. The functions does not return anything explicitly'''
    tarfilename = CACHE_DIR / branch / f'{repo}.db.tar.gz'
    actual_branch = branch.removeprefix('arm-')
    with tempfile.TemporaryDirectory(prefix='pacmandb') as tmp_dir:
        tmp_path = Path(tmp_dir)
        with tarfile.open(tarfilename, 'r:gz') as f:
            f.extractall(path=tmp_path)

        for dir in tmp_path.glob('*'):
            *package, version, subversion = dir.name.split('-')
            package_name = '-'.join(package)
            version = f'{version}-{subversion}'
            if not packages.get(package_name, None):
                packages[package_name] = {}
                for branch_name in BRANCHES:
                    packages[package_name][branch_name] = { 'version': '' } 
            packages[package_name][actual_branch]['version'] = version
            packages[package_name][actual_branch]['repo'] = repo

            # Is this a Manjaro package? Find out checking the PACKAGER
            with open(dir / 'desc','r') as file:
                descriptor = file.read().strip().split('\n\n')
            for field in descriptor:
                property, *value = field.split('\n')
                property = property.strip('%')
                if property != 'PACKAGER':
                    continue
                packages[package_name][actual_branch]['Manjaro'] = bool(value[0].lower().count('manjaro'))
                break

def sync_remote(branches: list, repos: list, arch:str) -> bool:
    '''Retrieve packages' databases for the given branches and repos from a mirror (defined globally).'''
    changes = False

    # Iterate branches
    for branch in branches:
        # If branch does not yet have a folder, create it
        branch_cache = CACHE_DIR / branch
        if not branch_cache.exists():
            print(f'Creating {branch_cache}')
            branch_cache.mkdir()
        
        # Iterate repos for this branch
        for repo in repos:
            remote_filename = f'{repo}.db.tar.gz'
            
            # Get remote database file's datetime
            remote_url = f'{MIRROR}/{branch}/{repo}/{arch}/{remote_filename}'
            print(f'Remote head {branch}:{repo}')
            r = requests.head(url=remote_url, timeout=30)
            remote_datetime = parsedate(r.headers['Last-Modified']).astimezone()
            print(f'Remote file\'s datetime {remote_datetime}')
            
            # Get local file's datetime
            localfilename = branch_cache / remote_filename
            localfilename_datetime = datetime.fromtimestamp(localfilename.stat().st_mtime).astimezone() \
                if localfilename.exists() else None
            print(f'Local file\'s datetime {localfilename_datetime}')
            # If local database file is older than remote's, then skip to the next repo
            if localfilename_datetime and localfilename_datetime >= remote_datetime:
                continue
            
            # Download the database file for this repo and branch
            print('Downloading remote file')
            r = requests.get(url=remote_url, timeout=30)
            if r.ok:
                changes = True
                print('Writing downloaded file to disk')
                with open(localfilename, 'wb') as f:
                    f.write(r.content)
            else:
                print(f'Download failed for {remote_url}')
    return changes

def create_package_list(branches: list, repos: list, arch: str) -> dict:
    '''Calls the function to create in-memory packages' data from the given branches and repos.'''
    print('Composing new package list...')
    packages = {}
    for branch_name in branches:
        for repo_name in repos:
            read_packages(packages, repo=repo_name, branch=branch_name)

    for package_name, package in packages.items():
        s = bool(package['stable']['version'])
        t = bool(package['testing']['version'])
        u = bool(package['unstable']['version'])

        if (not s and t and not u) or (s and not t and u):
            packages[package_name]['error'] = True

        if s and not u:
            packages[package_name]['eol'] = True

        if not s and u:
            packages[package_name]['new'] = True

        for branch_name in branches:
            branch_name = branch_name.removeprefix('arm-')
            if package[branch_name].get('Manjaro', False):
                packages[package_name]['manjaro'] = True
                break
        
        pattern = r'^linux\d+$' if arch == 'x86_64' else r'^linux-[^-]+$'
        if re.search(pattern, package_name):
            packages[package_name]['kernels'] = True

    return {name: packages[name] for name in sorted(packages)}

def save_to_database(packages: dict, arch: str) -> None:
    '''Saves in-memory packages' data to database on disk'''
    db = f'{arch}.json'
    print(f'Saving data to database {db}')
    last_update = f"{' '.join(datetime.now(UTC).isoformat().split('T'))} (UTC)"
    database = {
        'packages': packages,
        'last_update': last_update,
    }

    with open(db, 'w') as db_file:
        json.dump(database, db_file)

    return

if __name__ == '__main__':
    if not CACHE_DIR.exists():
        CACHE_DIR.mkdir()
    
    reload = False
    for arch in ARCHS:
        if sync_remote(MIRROR_BRANCHES[arch], REPOS[arch], arch):
            reload = True
            packages = create_package_list(MIRROR_BRANCHES[arch], REPOS[arch], arch)
            save_to_database(packages, arch)

    if reload:
        argv = ['systemctl', 'reload', 'gunicorn_branch']
        subprocess.run(argv)
