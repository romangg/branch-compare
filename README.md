# Branch compare for Manjaro

Small web tool to show package's versions in each branch. It's a very simple tool on purpose.

This tool accepts a regex (regular expression) and shows all the packages whose name match the regex. For a single package name, the regex would be `^packagename$`. For example: `^vim$`

It also accepts these precalculated searches:
- `#kernels`: Show kernel packages
- `#new`    : Show new packages (packages that have not yet hit stable)
- `#eol`    : Show packages in stable that don't have a replacement in unstable/testing
- `#error`  : Show packages that seems to be an error (for example, packages only in testing branch)
- `#manjaro`: Show packages from Manjaro devs.

Remember that normal package path is unstable --> testing --> stable

Project is made in Python using Flask and requests for the backend and TailwindCSS for the frontend.

To run a local copy:
```
# Clone the repo and `cd` to it
git clone https://gitlab.com/cfinnberg/branch-compare.git
cd branch_compare

# Make a Python virtual environment and activate it
python -m venv venv
source venv/bin/activate    # or activate.fish or activate.csh

# Update pip itself and install required Python modules
pip install -U pip
pip install -r requirements.txt

# Change the mirror to update from if necessary (in update_db.py).
# Sync the databases with selected mirror.
python update_db.py

# Run the webtool
flask run

# Open the webtool in the browser. Address is http://127.0.0.1:5000/branch_compare
```

A Systemd service unit file and a Caddy Web server config file are provided as examples for a system service. Check /resources folder.

Please open an issue if you find any bugs or problems. Thanks!
