#!/bin/bash
BASEDIR=$(dirname $(realpath $BASH_SOURCE))
APPNAME=$(basename $BASEDIR)

filename=$(basename -- "$BASH_SOURCE")
filename="${filename%.*}"

cd $BASEDIR/$APPNAME

$BASEDIR/venv/bin/python $filename.py >> $BASEDIR/logs/$filename.log 2>&1

